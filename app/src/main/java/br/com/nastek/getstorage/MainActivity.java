package br.com.nastek.getstorage;

import android.nfc.Tag;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private static String sdCardPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView txt = (TextView) findViewById(R.id.sdpath);
        final Button btn = (Button) findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt.setText(getStoragePath());
            }
        });

        getStoragePath();
    }

    private String getStoragePath(){
        if (Environment.isExternalStorageEmulated()){
          return "StorageEmulated";
       }else if(Environment.isExternalStorageRemovable()){
            Toast.makeText(this, "Possui cartao!!!", Toast.LENGTH_LONG).show();
            return "Storage SD Card";
       }

       return  "asdasd";
    }

}
